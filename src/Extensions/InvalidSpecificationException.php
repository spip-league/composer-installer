<?php

namespace SpipLeague\Composer\Extensions;

/**
 * @since 0.7.0
 */
class InvalidSpecificationException extends \DomainException {}
