<?php

namespace SpipLeague\Test\Composer;

use Composer;
use Composer\Package\Package;
use Composer\Package\RootPackage;
use InvalidArgumentException;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\SpipInstaller;
use SpipLeague\Composer\SpipPaths;

#[CoversClass(SpipInstaller::class)]
class SpipInstallerTest extends TestCase
{
    private SpipInstaller $installer;

    protected function setUp(): void
    {
        $io = new Composer\IO\NullIO();
        $composer = Composer\Factory::create($io);
        $package = new RootPackage('spip/spip', '1.0', '1.0.0.0');
        $package->setExtra([
            'spip' => [
                'template' => 'spip/default-template',
                'extensions' => ['spip/mandatory'],
            ],
        ]);
        $composer->setPackage($package);

        $this->installer = new SpipInstaller($io, $composer);
    }

    public static function dataPluginTypes()
    {
        return [
            'library' => [
                'expected' => false,
                'type' => 'library',
            ],
            'classic' => [
                'expected' => true,
                'type' => 'spip-classic',
            ],
            'ecrire' => [
                'expected' => true,
                'type' => 'spip-ecrire',
            ],
            'prive' => [
                'expected' => true,
                'type' => 'spip-prive',
            ],
            'plugin' => [
                'expected' => true,
                'type' => 'spip-plugin',
            ],
        ];
    }

    #[DataProvider('dataPluginTypes')]
    public function testSupports($expected, $type)
    {
        $package = new Package('spip/test', '1.0.0', '1.0.0.0');
        $package->setType($type);

        $this->assertEquals($expected, $this->installer->supports($package->getType()));
    }

    public static function dataPluginPaths()
    {
        return [
            'library' => [
                'expected' => dirname(__DIR__, 1) . '/vendor/spip/test',
                'name' => 'spip/test',
                'type' => 'library',

            ],
            'classic' => [
                'expected' => './tmp/__spip_classic__',
                'name' => 'spip/test',
                'type' => 'spip-classic',
            ],
            'ecrire' => [
                'expected' => './' . SpipPaths::BACK_OFFICE,
                'name' => 'spip/test',
                'type' => 'spip-ecrire',
            ],
            'prive' => [
                'expected' => './' . SpipPaths::PRIVATE_TEMPLATE,
                'name' => 'spip/test',
                'type' => 'spip-prive',
            ],
            'plugin' => [
                'expected' => './' . SpipPaths::PLUGINS . '/spip/test',
                'name' => 'spip/test',
                'type' => 'spip-plugin',
            ],
            'extension' => [
                'expected' => './' . SpipPaths::EXTENSIONS . '/spip/mandatory',
                'name' => 'spip/mandatory',
                'type' => 'spip-plugin',
            ],
            'template' => [
                'expected' => './' . SpipPaths::TEMPLATE,
                'name' => 'spip/default-template',
                'type' => 'spip-plugin',
            ],
        ];
    }

    #[DataProvider('dataPluginPaths')]
    public function testGetInstallPath($expected, $name, $type)
    {
        $package = new Package($name, '1.0.0', '1.0.0.0');
        $package->setType($type);

        $this->assertSame($expected, $this->installer->getInstallPath($package));
    }

    public function testUnsupportedType()
    {
        $this->expectException(InvalidArgumentException::class);
        $package = new Package('spip/test', '1.0.0', '1.0.0.0');
        $package->setType('spip-lang');

        $this->installer->getInstallPath($package);
    }
}
