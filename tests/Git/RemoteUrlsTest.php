<?php

namespace SpipLeague\Test\Composer\Git;

use Composer\IO\NullIO;
use Composer\Util\ProcessExecutor;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Git\RemoteUrls;

#[CoversClass(RemoteUrls::class)]
class RemoteUrlsTest extends TestCase
{
    private RemoteUrls $changer;

    protected function setUp(): void
    {
        $this->changer = new RemoteUrls(new ProcessExecutor(new NullIO()));
    }

    public static function dataHttpsToSsh()
    {
        return [
            'empty-url' => [
                'expected' => '',
                'url' => '',
            ],
            'matching-spip-url' => [
                'expected' => 'git@git.spip.net:owner/package.git',
                'url' => 'https://git.spip.net/owner/package.git',
            ],
            'non-matching-spip-url' => [
                'expected' => 'git@git.spip.net:owner/package.git',
                'url' => 'git@git.spip.net:owner/package.git',
            ],
        ];
    }

    #[DataProvider('dataHttpsToSsh')]
    public function testToSsh($expected, $url)
    {
        $this->assertSame($expected, $this->changer->toSsh($url));
    }

    public static function dataSshToHttps()
    {
        return [
            'empty-url' => [
                'expected' => '',
                'url' => '',
            ],
            'matching-spip-url' => [
                'expected' => 'https://git.spip.net/owner/package.git',
                'url' => 'git@git.spip.net:owner/package.git',
            ],
            'non-matching-spip-url' => [
                'expected' => 'https://git.spip.net/owner/package.git',
                'url' => 'https://git.spip.net/owner/package.git',
            ],
        ];
    }

    #[DataProvider('dataSshToHttps')]
    public function testToHttps($expected, $url)
    {
        $this->assertSame($expected, $this->changer->toHttps($url));
    }

    public function testGetRemote()
    {
        $this->assertSame('git -C test remote get-url origin', $this->changer->getRemote('test'));
    }

    public function testSetRemote()
    {
        $this->assertSame('git -C test remote set-url origin test-url', $this->changer->setRemote('test', 'test-url'));
    }

    public function testGetProcessor()
    {
        $this->assertInstanceOf(ProcessExecutor::class, $this->changer->getProcessor());
    }
}
