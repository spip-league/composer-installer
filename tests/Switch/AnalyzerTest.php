<?php

namespace SpipLeague\Test\Composer\Switch;

use Composer\Composer;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\IO\NullIO;
use Composer\Json\JsonFile;
use Composer\Package\RootPackage;
use Composer\Util\Filesystem;
use Composer\Util\HttpDownloader;
use Composer\Util\Loop;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Analyzer;
use SpipLeague\Composer\Switch\Operation\AddToRequire;
use SpipLeague\Composer\Switch\Operation\AddToSpipExtraExtensions;
use SpipLeague\Composer\Switch\Operation\RemoveSpecification;
use SpipLeague\Test\Composer\Fixtures\CollectionMock;
use SpipLeague\Test\Composer\Fixtures\SpecificationMock;

#[CoversClass(Analyzer::class)]
#[CoversClass(AddToRequire::class)]
#[CoversClass(AddToSpipExtraExtensions::class)]
#[CoversClass(RemoveSpecification::class)]
class AnalyzerTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/AnalyzerTest';
        $this->filesystem->emptyDirectory($this->tmpDir);

        $rootPackage = new RootPackage('test/test', '1', '1');
        $this->composer = new Composer();
        $this->composer->setPackage($rootPackage);
        $this->composer->setConfig(new Config());
        $this->composer->getConfig()
            ->setConfigSource(new JsonConfigSource(new JsonFile($this->tmpDir . '/AnalyzerTest.json')));
        $this->composer->getConfig()
            ->getConfigSource()
            ->addConfigSetting('test', 'test');
        $this->composer->setLoop(new Loop(new HttpDownloader(new NullIO(), $this->composer->getConfig())));
    }

    protected function tearDown(): void
    {
        unset($this->composer);
        unset($this->collection);

        $this->filesystem->unlink($this->tmpDir . '/AnalyzerTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataForward()
    {
        return [
            'switch vendor/prefix' => [
                'expected' => [
                    new AddToSpipExtraExtensions('vendor/prefix'),
                    new AddToRequire('vendor/prefix', '^1.0'),
                    new RemoveSpecification('prefix'),
                ],
            ],
        ];
    }

    #[DataProvider('dataForward')]
    public function testForward($expected)
    {
        // Given
        $collection = new CollectionMock('');
        $collection[] = new SpecificationMock();
        $analyzer = new Analyzer($collection, $this->composer);
        $actual = [];

        // When
        foreach ($analyzer->forward() as $operation) {
            $actual[] = $operation;
        }

        // Then
        $this->assertEquals($expected, $actual);
    }

    public static function dataBaack()
    {
        return [
            'nothing' => [
                'expected' => [],
            ],
        ];
    }

    #[DataProvider('dataBaack')]
    public function testBack($expected)
    {
        // Given
        $collection = new CollectionMock('');
        $analyzer = new Analyzer($collection, $this->composer);
        $actual = [];

        // When
        foreach ($analyzer->back() as $operation) {
            $actual[] = $operation;
        }

        // Then
        $this->assertEquals($expected, $actual);
    }
}
