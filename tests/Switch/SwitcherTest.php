<?php

namespace SpipLeague\Test\Composer\Switch;

use Composer\Composer;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Extensions\CollectionInterface;
use SpipLeague\Composer\Switch\Switcher;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;
use SpipLeague\Test\Composer\Fixtures\OperationMock;

#[CoversClass(Switcher::class)]
class SwitcherTest extends TestCase
{
    private Composer $composer;

    private CollectionInterface $collection;

    protected function setUp(): void
    {
        $this->composer = new Composer();
        $this->collection = new CollectionDummy();
    }

    protected function tearDown(): void
    {
        unset($this->composer);
        unset($this->collection);
    }

    public static function dataToAndSwitch()
    {
        return [
            'nothing' => [
                'expected' => [
                    '<comment>Operating distribution</comment>',
                    '<comment>Operating extensions</comment>',
                    '<comment>Operating requires</comment>',
                    '<comment>Operating directories</comment>',
                ],
                'operation' => new OperationMock('', 'distribution', \false, ''),
            ],
            'one distribution switch' => [
                'expected' => [
                    '<comment>Operating distribution</comment>',
                    '<comment>done</comment>',
                    '<comment>Operating extensions</comment>',
                    '<comment>Operating requires</comment>',
                    '<comment>Operating directories</comment>',
                ],
                'operation' => new OperationMock('', 'distribution', \true, 'done'),
            ],
        ];
    }

    #[DataProvider('dataToAndSwitch')]
    public function testToAndFlush($expected, $operation)
    {
        // Given
        $switcher = new Switcher($this->collection, $this->composer);
        $actual = [];

        // When
        $switcher->to($operation);
        foreach ($switcher->flush(fn() => $this->composer) as $message) {
            $actual[] = $message;
        }

        // Then
        $this->assertSame($expected, $actual);
    }
}
