<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Package\Link;
use Composer\Package\RootPackage;
use Composer\Semver\Constraint\Constraint;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Composer\Switch\Operation\RemoveFromRequire;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;

#[CoversClass(RemoveFromRequire::class)]
class RemoveFromRequireTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/RemoveFromRequireTest';
        $this->filesystem->emptyDirectory($this->tmpDir);

        $rootPackage = new RootPackage('test/test', '1', '1');
        $rootPackage->setRequires(
            ['vendor/exist' => new Link('test/test', 'vendor/exist', new Constraint('>=', '1.0'))],
        );

        $this->composer = new Composer();
        $this->composer->setConfig(new Config());
        $this->composer->getConfig()
            ->setConfigSource(new JsonConfigSource(new JsonFile($this->tmpDir . '/RemoveFromRequireTest.json')));
        $this->composer->setPackage($rootPackage);
        $this->composer->getConfig()
            ->getConfigSource()
            ->addLink('require', 'vendor/exist', '^1.0');
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/RemoveFromRequireTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'not-exist' => [
                'expected' => \null,
                'vendorName' => 'vendor/not-exist',
            ],
            'exist' => [
                'expected' => OperationInterface::class,
                'vendorName' => 'vendor/exist',
            ],

        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $vendorName)
    {
        // Given
        $operation = new RemoveFromRequire($vendorName);

        // When
        $actual = $operation->mark(new CollectionDummy(), $this->composer);

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'exist' => [
                'expected' => 'require vendor/exist removed',
                'vendorName' => 'vendor/exist',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $vendorName)
    {
        // Given
        $operation = new RemoveFromRequire($vendorName);

        // When
        $actual = $operation->do(new CollectionDummy(), $this->composer);

        // Then
        $this->assertSame($expected, $actual);
    }
}
