<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Package\RootPackage;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\AddToSpipExtraExtensions;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;

#[CoversClass(AddToSpipExtraExtensions::class)]
class AddToSpipExtraExtensionsTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/AddToSpipExtraExtensionsTest';
        $this->filesystem->emptyDirectory($this->tmpDir);

        $this->composer = new Composer();
        $this->composer->setConfig(new Config());
        $this->composer->getConfig()
            ->setConfigSource(new JsonConfigSource(new JsonFile($this->tmpDir . '/AddToSpipExtraExtensionsTest.json')));

        $rootPackage = new RootPackage('test/test', '1', '1');
        $this->composer->setPackage($rootPackage);
        $this->composer->getConfig()
            ->getConfigSource()
            ->addProperty('extra.spip.extensions', ['vendor/exist']);
        $this->composer->getPackage()
            ->setExtra(['spip' => ['extensions' => ['vendor/exist']]]);
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/AddToSpipExtraExtensionsTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'exist' => [
                'expected' => \null,
                'vendorName' => 'vendor/exist',
            ],
            'not-exist' => [
                'expected' => OperationInterface::class,
                'vendorName' => 'vendor/not-exist',
            ],
        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $vendorName)
    {
        // Given
        $operation = new AddToSpipExtraExtensions($vendorName);

        // When
        $actual = $operation->mark(new CollectionDummy(), $this->composer);

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'added' => [
                'expected' => 'extensions vendor/prefix added',
                'vendorName' => 'vendor/prefix',
            ],
            'already-exists' => [
                'expected' => 'nothing to do',
                'vendorName' => 'vendor/exist',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $vendorName)
    {
        // Given
        $operation = new AddToSpipExtraExtensions($vendorName);

        // When
        $actual = $operation->do(new CollectionDummy(), $this->composer);

        // Then
        $this->assertSame($expected, $actual);
    }
}
