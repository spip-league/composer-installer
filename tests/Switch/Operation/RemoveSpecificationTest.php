<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Composer\Switch\Operation\RemoveSpecification;
use SpipLeague\Test\Composer\Fixtures\CollectionMock;
use SpipLeague\Test\Composer\Fixtures\SpecificationMock;

#[CoversClass(RemoveSpecification::class)]
class RemoveSpecificationTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    private CollectionMock $collection;

    protected function setUp(): void
    {
        $this->composer = new Composer();
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/RemoveSpecificationTest';
        $this->filesystem->emptyDirectory($this->tmpDir);
        $this->collection = new CollectionMock($this->tmpDir . '/RemoveSpecificationTest.json');
        $this->collection[] = new SpecificationMock('exist');
        \file_put_contents(
            $this->tmpDir . '/RemoveSpecificationTest.json',
            '{"exist":{"path":"path","source":"source","branch":"1.0"}}',
        );
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/RemoveSpecificationTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'not-exist' => [
                'expected' => \null,
                'prefix' => 'not-exist',
            ],
            'exist' => [
                'expected' => OperationInterface::class,
                'prefix' => 'exist',
            ],
        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $prefix)
    {
        // Given
        $operation = new RemoveSpecification($prefix);

        // When
        $actual = $operation->mark($this->collection, $this->composer);

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'exist' => [
                'expected' => 'prefix exist removed',
                'prefix' => 'exist',
            ],
            'not-exist' => [
                'expected' => 'nothing to do',
                'prefix' => 'not-exist',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $prefix)
    {
        // Given
        $operation = new RemoveSpecification($prefix);

        // When
        $actual = $operation->do($this->collection, $this->composer);

        // Then
        $this->assertSame($expected, $actual);
    }
}
