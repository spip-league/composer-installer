<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Package\Link;
use Composer\Package\RootPackage;
use Composer\Semver\Constraint\Constraint;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\AddToRequire;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;

#[CoversClass(AddToRequire::class)]
class AddToRequireTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/AddToRequireTest';
        $this->filesystem->emptyDirectory($this->tmpDir);

        $rootPackage = new RootPackage('test/test', '1', '1');
        $rootPackage->setRequires(
            ['vendor/exist' => new Link('test/test', 'vendor/exist', new Constraint('>=', '1.0'))],
        );

        $this->composer = new Composer();
        $this->composer->setConfig(new Config());
        $this->composer->getConfig()
            ->setConfigSource(new JsonConfigSource(new JsonFile($this->tmpDir . '/AddToRequireTest.json')));
        $this->composer->setPackage($rootPackage);
        $this->composer->getConfig()
            ->getConfigSource()
            ->addLink('require', 'vendor/exist', '^1.0');
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/AddToRequireTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataDo()
    {
        return [
            'exist' => [
                'expected' => 'require vendor/exist:^2.0 added.',
                'vendorName' => 'vendor/exist',
                'constraint' => '^2.0',
            ],
            'new' => [
                'expected' => 'require vendor/new:^1.0 added.',
                'vendorName' => 'vendor/new',
                'constraint' => '^1.0',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $vendorName, $constraint)
    {
        // Given
        $operation = new AddToRequire($vendorName, $constraint);

        // When
        $actual = $operation->do(new CollectionDummy(), $this->composer);
        $actualContent = \json_decode(\file_get_contents($this->tmpDir . '/AddToRequireTest.json'), \true);
        $actualContent = $actualContent['require'][$vendorName] ?? \null;

        // Then
        $this->assertSame($expected, $actual);
        $this->assertSame($constraint, $actualContent);
    }
}
