<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Composer\Switch\Operation\RemoveDirectory;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;

#[CoversClass(RemoveDirectory::class)]
class RemoveDirectoryTest extends TestCase
{
    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/RemoveDirectoryTest';
        $this->filesystem->emptyDirectory($this->tmpDir . '/empty', \true);
        $this->filesystem->emptyDirectory($this->tmpDir . '/not-empty', \true);
        \touch($this->tmpDir . '/not-empty/testFile');
    }

    protected function tearDown(): void
    {
        if (\file_exists($this->tmpDir . '/not-empty/testFile')) {
            $this->filesystem->unlink($this->tmpDir . '/not-empty/testFile');
        }
        $this->filesystem->rmdir($this->tmpDir . '/not-empty');
        if (\is_dir($this->tmpDir . '/empty')) {
            $this->filesystem->rmdir($this->tmpDir . '/empty');
        }
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'not-exist' => [
                'expected' => \null,
                'directory' => 'not-exist',
            ],
            'exist' => [
                'expected' => OperationInterface::class,
                'directory' => 'empty',
            ],
        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $directory)
    {
        // Given
        $operation = new RemoveDirectory($this->filesystem, $this->tmpDir . '/' . $directory);

        // When
        $actual = $operation->mark(new CollectionDummy(), new Composer());

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'not-exists' => [
                'expected' => 'not-exist removed',
                'directory' => 'not-exist',
                'ifEmpty' => \false,
            ],
            'not-empty' => [
                'expected' => 'not-empty error on removing ...',
                'directory' => 'not-empty',
                'ifEmpty' => \true,
            ],
            'empty' => [
                'expected' => 'empty removed',
                'directory' => 'empty',
                'ifEmpty' => \true,
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $directory, $ifEmpty)
    {
        // Given
        $operation = new RemoveDirectory($this->filesystem, $this->tmpDir . '/' . $directory, $ifEmpty);

        // When
        $actual = $operation->do(new CollectionDummy(), new Composer());

        // Then
        $this->assertSame($this->tmpDir . '/' . $expected, $actual);
    }
}
