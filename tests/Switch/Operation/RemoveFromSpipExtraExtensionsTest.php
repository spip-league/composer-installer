<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Config;
use Composer\Config\JsonConfigSource;
use Composer\Json\JsonFile;
use Composer\Package\RootPackage;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Composer\Switch\Operation\RemoveFromSpipExtraExtensions;
use SpipLeague\Test\Composer\Fixtures\CollectionDummy;

#[CoversClass(RemoveFromSpipExtraExtensions::class)]
class RemoveFromSpipExtraExtensionsTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    protected function setUp(): void
    {
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/RemoveFromSpipExtraExtensionsTest';
        $this->filesystem->emptyDirectory($this->tmpDir);

        $this->composer = new Composer();
        $this->composer->setConfig(new Config());
        $this->composer->getConfig()
            ->setConfigSource(
                new JsonConfigSource(new JsonFile($this->tmpDir . '/RemoveFromSpipExtraExtensionsTest.json')),
            );

        $rootPackage = new RootPackage('test/test', '1', '1');
        $this->composer->setPackage($rootPackage);
        $this->composer->getConfig()
            ->getConfigSource()
            ->addProperty('extra.spip.extensions', ['vendor/exist']);
        $this->composer->getPackage()
            ->setExtra(['spip' => ['extensions' => ['vendor/exist']]]);
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/RemoveFromSpipExtraExtensionsTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'exist' => [
                'expected' => OperationInterface::class,
                'vendorName' => 'vendor/exist',
            ],
            'not-exist' => [
                'expected' => \null,
                'vendorName' => 'vendor/not-exist',
            ],
        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $vendorName)
    {
        // Given
        $operation = new RemoveFromSpipExtraExtensions($vendorName);

        // When
        $actual = $operation->mark(new CollectionDummy(), $this->composer);

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'removed' => [
                'expected' => 'extensions vendor/exist removed',
                'vendorName' => 'vendor/exist',
            ],
            'already-absent' => [
                'expected' => 'nothing to do',
                'vendorName' => 'vendor/prefix',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $vendorName)
    {
        // Given
        $operation = new RemoveFromSpipExtraExtensions($vendorName);

        // When
        $actual = $operation->do(new CollectionDummy(), $this->composer);

        // Then
        $this->assertSame($expected, $actual);
    }
}
