<?php

namespace SpipLeague\Test\Composer\Switch\Operation;

use Composer\Composer;
use Composer\Util\Filesystem;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Switch\Operation\AddSpecification;
use SpipLeague\Composer\Switch\Operation\OperationInterface;
use SpipLeague\Test\Composer\Fixtures\CollectionMock;
use SpipLeague\Test\Composer\Fixtures\SpecificationMock;
use SpipLeague\Test\Composer\Fixtures\SpecificationStub;

#[CoversClass(AddSpecification::class)]
class AddSpecificationTest extends TestCase
{
    private Composer $composer;

    private string $tmpDir;

    private Filesystem $filesystem;

    private CollectionMock $collection;

    protected function setUp(): void
    {
        $this->composer = new Composer();
        $this->filesystem = new Filesystem();
        $this->tmpDir = \sys_get_temp_dir() . '/AddSpecificationTest';
        $this->filesystem->emptyDirectory($this->tmpDir);
        $this->collection = new CollectionMock($this->tmpDir . '/AddSpecificationTest.json');
        $this->collection[] = new SpecificationMock('exist');
        \file_put_contents(
            $this->tmpDir . '/AddSpecificationTest.json',
            '{"exist":{"path":"path","source":"https://git-server/vendor/exist.git","branch":"1.0","tag":"v1.0.0"}}',
        );
    }

    protected function tearDown(): void
    {
        $this->filesystem->unlink($this->tmpDir . '/AddSpecificationTest.json');
        $this->filesystem->rmdir($this->tmpDir);
    }

    public static function dataMark()
    {
        return [
            'not-exist' => [
                'expected' => OperationInterface::class,
                'prefix' => 'not-exist',
            ],
            'exist' => [
                'expected' => \null,
                'prefix' => 'exist',
            ],
        ];
    }

    #[DataProvider('dataMark')]
    public function testMark($expected, $prefix)
    {
        // Given
        $operation = new AddSpecification(new SpecificationStub($prefix));

        // When
        $actual = $operation->mark($this->collection, $this->composer);

        // Then
        if ($expected === null) {
            $this->assertNull($actual);
        } else {
            $this->assertInstanceOf($expected, $actual);
        }
    }

    public static function dataDo()
    {
        return [
            'exist' => [
                'expected' => 'exist added to ',
                'prefix' => 'exist',
            ],
            'not-exist' => [
                'expected' => 'not-exist added to ',
                'prefix' => 'not-exist',
            ],
        ];
    }

    #[DataProvider('dataDo')]
    public function testDo($expected, $prefix)
    {
        // Given
        $operation = new AddSpecification(new SpecificationStub($prefix));

        // When
        $actual = $operation->do($this->collection, $this->composer);

        // Then
        $this->assertStringStartsWith($expected, $actual);
    }
}
