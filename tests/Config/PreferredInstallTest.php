<?php

namespace SpipLeague\Test\Composer\Config;

use Composer\Composer;
use Composer\Config;
use Composer\Package\Link;
use Composer\Package\RootPackage;
use Composer\Semver\Constraint\Constraint;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Config\PreferredInstall;

#[CoversClass(PreferredInstall::class)]
class PreferredInstallTest extends TestCase
{
    private PreferredInstall $preferredInstall;

    private Config $config;

    private RootPackage $package;

    protected function setUp(): void
    {
        $this->config = new Config();
        $this->package = new RootPackage('spip/spip', '1.0', '1.0.0.0');
        $composer = new Composer();
        $composer->setConfig($this->config);
        $composer->setPackage($this->package);
        $this->preferredInstall = new PreferredInstall($composer);
    }

    public static function dataGetFromConfig()
    {
        return [
            'spip/*' => [
                'expected' => [
                    dirname(__DIR__, 2) . '/plugins-dist/spip/mandatory',
                    dirname(__DIR__, 2) . '/squelettes-dist',
                    'vendor/spip/package',
                    dirname(__DIR__, 2) . '/prive',
                    dirname(__DIR__, 2) . '/ecrire',
                ],
                'preferred' => ['spip/*' => 'source'],
                'requires' => [
                    'mandatory' => new Link('mandatory', 'spip/mandatory', new Constraint('=', '*')),
                    'template' => new Link('template', 'spip/default-template', new Constraint('=', '*')),
                    'package' => new Link('package', 'spip/package', new Constraint('=', '*')),
                    'other-package' => new Link('other-package', 'other/package', new Constraint('=', '*')),
                    'private-template' => new Link('private-template', 'spip/private-template', new Constraint(
                        '=',
                        '*',
                    )),
                    'back-office' => new Link('back-office', 'spip/back-office', new Constraint('=', '*')),
                ],
                'extra' => [
                    'spip' => [
                        'template' => 'spip/default-template',
                        'extensions' => ['spip/mandatory'],
                        'private_template' => 'spip/private-template',
                        'back_office' => 'spip/back-office',
                    ],
                ],
            ],
            'empty' => [
                'expected' => [],
                'preferred' => \null,
                'requires' => [],
                'extra' => [],
            ],
            'two-preferred' => [
                'expected' => [
                    dirname(__DIR__, 2) . '/plugins-dist/one/mandatory1',
                    dirname(__DIR__, 2) . '/squelettes-dist',
                ],
                'preferred' => ['one/*' => 'source', 'two/*' => 'source'],
                'requires' => [
                    'mandatory1' => new Link('mandatory', 'one/mandatory1', new Constraint('=', '*')),
                    'template2' => new Link('template', 'two/template2', new Constraint('=', '*')),
                    'other-package' => new Link('other-package', 'other/package', new Constraint('=', '*')),
                ],
                'extra' => [
                    'spip' => [
                        'template' => 'two/template2',
                        'extensions' => ['one/mandatory1'],
                    ],
                ],
            ],
        ];
    }

    #[DataProvider('dataGetFromConfig')]
    public function testGetFromConfig($expected, $preferred, $requires, $extra)
    {
        $this->package->setRequires($requires);
        $this->package->setExtra($extra);
        $this->config->merge(['config' => ['preferred-install' => $preferred]]);
        $this->assertSame($expected, $this->preferredInstall->getFromConfig());
    }
}
