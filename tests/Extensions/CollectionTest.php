<?php

namespace SpipLeague\Test\Composer\Extensions;

use Composer\IO\NullIO;
use Composer\Util\ProcessExecutor;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Extensions\InvalidSpecificationException;
use SpipLeague\Composer\Extensions\Collection;
use SpipLeague\Composer\Extensions\Specification;
use SpipLeague\Composer\Git\RemoteUrls;
use SpipLeague\Test\Composer\Fixtures\SpecificationStub;

#[CoversClass(Collection::class)]
#[CoversClass(Specification::class)]
#[CoversClass(RemoteUrls::class)]
class CollectionTest extends TestCase
{
    private SpecificationStub $validStub;

    private Collection $testCollection;

    private RemoteUrls $changer;

    protected function setUp(): void
    {
        $this->validStub = new SpecificationStub('valid', 'stub/valid', '1.0.0');
        $this->testCollection = new Collection([$this->validStub]);
        $this->changer = new RemoteUrls(new ProcessExecutor(new NullIO()));
    }

    public function testInvalidSpecification()
    {
        // Given
        $this->expectException(InvalidSpecificationException::class);
        $this->expectExceptionMessage('A collection must contain at least one valid specification.');

        // When
        new Collection([new SpecificationStub()]);

        // Then
        // An exception is thrown
    }

    public function testInvalidSpecificationByArrayAccess()
    {
        // Given
        $this->expectException(InvalidSpecificationException::class);
        $this->expectExceptionMessage('A collection must only contain valid specifications.');

        // When
        $this->testCollection[] = 'invalid';

        // Then
        // An exception is thrown
    }

    public static function dataGetByArrayAccess()
    {
        return [
            'notexists' => [
                'expected' => null,
                'prefix' => 'notexists',
            ],
            'exists' => [
                'expected' => 'stub/valid',
                'prefix' => 'valid',
            ],
        ];
    }

    #[DataProvider('dataGetByArrayAccess')]
    public function testGetByArrayAccess($expected, $prefix)
    {
        // Given
        // When
        $actual = $this->testCollection[$prefix];

        // Then
        $this->assertEquals($expected, $actual?->computeVendorName());
    }

    public static function dataUnsetByArrayAccess()
    {
        return [
            'notexists' => [
                'expected' => 1,
                'prefix' => 'notexists',
            ],
            'exists' => [
                'expected' => 0,
                'prefix' => 'valid',
            ],
        ];
    }

    #[DataProvider('dataUnsetByArrayAccess')]
    public function testUnsetByArrayAccess($expected, $prefix)
    {
        // Given
        // When
        unset($this->testCollection[$prefix]);

        // Then
        $this->assertCount($expected, $this->testCollection);
    }

    public static function dataSetByArrayAccess()
    {
        return [
            'new' => [
                'expected' => 2,
                'prefix' => 'test',
            ],
            'existing' => [
                'expected' => 1,
                'prefix' => 'valid',
            ],
        ];
    }

    #[DataProvider('dataSetByArrayAccess')]
    public function testSetByArrayAccess($expected, $prefix)
    {
        // Given
        // When
        $this->testCollection[] = new SpecificationStub($prefix);

        // Then
        $this->assertCount($expected, $this->testCollection);
        $this->assertArrayHasKey($prefix, $this->testCollection);
    }

    public function testJsonSerialization()
    {
        // Given
        // When
        $this->testCollection[] = new SpecificationStub('test');
        $actual = \json_encode($this->testCollection);

        // Then
        $this->assertSame(
            '{"valid":{"path":"path","source":"source"},"test":{"path":"path","source":"source"}}',
            $actual,
        );
    }

    public static function dataFromJsonFile()
    {
        return [
            'two' => [
                'expected' => 2,
                'file' => __DIR__ . '/../Fixtures/two.json',
                'prefix' => 'one',
            ],
        ];
    }

    #[DataProvider('dataFromJsonFile')]
    public function testFromJsonFile($expected, $file, $prefix)
    {
        // Given
        // When
        $actual = Collection::fromJsonFile($this->changer, $file);

        // Then
        $this->assertCount($expected, $actual);
        $this->assertTrue(isset($actual[$prefix]));
    }

    public function todo_testMissingFromJsonFile()
    {
        // Given
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessageMatches(',File "plugins-dist.json" is missing,');

        // When
        Collection::fromJsonFile($this->changer);

        // Then
        // An exception is thrown
    }

    public function testMalformedFromJsonFile()
    {
        // Given
        $this->expectException(\LogicException::class);
        $this->expectExceptionMessageMatches(',Fixtures/malformed.json" malformed,');

        // When
        Collection::fromJsonFile($this->changer, __DIR__ . '/../Fixtures/malformed.json');

        // Then
        // An exception is thrown
    }
}
