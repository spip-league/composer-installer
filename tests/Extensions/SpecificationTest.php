<?php

namespace SpipLeague\Test\Composer\Extensions;

use Composer\Composer;
use Composer\Factory as ComposerFactory;
use Composer\IO\NullIO;
use Composer\Package\RootPackage;
use Composer\Util\Platform;
use PHPUnit\Framework\Attributes\CoversClass;
use PHPUnit\Framework\Attributes\DataProvider;
use PHPUnit\Framework\TestCase;
use SpipLeague\Composer\Extensions\InvalidSpecificationException;
use SpipLeague\Composer\Extensions\Specification;
use SpipLeague\Composer\SpipPaths;
use SpipLeague\Test\Composer\Fixtures\RemoteUrlsStub;

#[CoversClass(Specification::class)]
class SpecificationTest extends TestCase
{
    private RootPackage $package;

    private Composer $composer;

    protected function setUp(): void
    {
        $this->package = new RootPackage('spip/spip', '1.0', '1.0.0.0');
        $this->composer = new Composer();
        $this->composer->setPackage($this->package);
    }

    public static function dataInvalidSpecification()
    {
        return [
            'empty prefix' => [
                'expected' => 'empty prefix is invalid',
                'prefix' => '',
                'fromJson' => [],
            ],
            'empty path' => [
                'expected' => 'empty path for "prefix" is invalid',
                'prefix' => 'prefix',
                'fromJson' => [],
            ],
            'empty source' => [
                'expected' => 'empty source for "prefix" is invalid',
                'prefix' => 'prefix',
                'fromJson' => [
                    'path' => SpipPaths::EXTENSIONS . '/prefix',
                ],
            ],
        ];
    }

    #[DataProvider('dataInvalidSpecification')]
    public function testInvalidSpecification($expected, $prefix, $fromJson)
    {
        // Given
        $this->expectException(InvalidSpecificationException::class);
        $this->expectExceptionMessage($expected);

        // When
        new Specification($prefix, $fromJson);

        // Then
        // An exception is thrown
    }

    public static function dataComputeVendorName()
    {
        return [
            'setChanger' => [
                'expected' => 'vendor/prefix',
                'changer' => true,
            ],
            'dontSetChanger' => [
                'expected' => '',
                'changer' => false,
            ],
        ];
    }

    #[DataProvider('dataComputeVendorName')]
    public function testComputeVendorName($expected, $changer)
    {
        // Given
        $specification = new Specification('prefix', [
            'path' => SpipPaths::EXTENSIONS . '/prefix',
            'source' => 'https://github.com/vendor/prefix.git',
        ]);
        if ($changer) {
            $specification->setChanger(new RemoteUrlsStub(
                'git@github.com:vendor/prefix.git',
                'https://github.com/vendor/prefix.git',
            ));
        }

        // When
        $actual = $specification->computeVendorName();

        // Then
        $this->assertSame($expected, $actual);
    }

    public static function dataComputeConstraint()
    {
        return [
            'noBranchOrTag' => [
                'expected' => '',
                'branchTag' => [],
            ],
            'branchOnly' => [
                'expected' => '^1.0.x-dev',
                'branchTag' => [
                    'branch' => '1.0',
                ],
            ],
            'tagOnly' => [
                'expected' => '^1.0',
                'branchTag' => [
                    'tag' => '1.0.0',
                ],
            ],
            'branchAndTag' => [
                'expected' => '^1.0',
                'branchTag' => [
                    'branch' => '1.0',
                    'tag' => 'v1.0.0',
                ],
            ],
        ];
    }

    #[DataProvider('dataComputeConstraint')]
    public function testComputeConstraint($expected, $branchTag)
    {
        // Given
        $specification = new Specification('prefix', \array_merge([
            'path' => SpipPaths::EXTENSIONS . '/prefix',
            'source' => 'https://github.com/vendor/prefix.git',
        ], $branchTag));
        $specification->setChanger(new RemoteUrlsStub(''));

        // When
        $actual = $specification->computeConstraint();

        // Then
        $this->assertSame($expected, $actual);
    }

    public static function dataJsonSerialization()
    {
        return [
            'noBranchOrTag' => [
                'expected' => '{"path":"plugins-dist\/prefix","source":"https:\/\/github.com\/vendor\/prefix.git"}',
                'branchTag' => [],
            ],
            'branchOnly' => [
                'expected' => '{"path":"plugins-dist\/prefix","source":"https:\/\/github.com\/vendor\/prefix.git","branch":"1.0"}',
                'branchTag' => [
                    'branch' => '1.0',
                ],
            ],
            'tagOnly' => [
                'expected' => '{"path":"plugins-dist\/prefix","source":"https:\/\/github.com\/vendor\/prefix.git","tag":"1.0.0"}',
                'branchTag' => [
                    'tag' => '1.0.0',
                ],
            ],
            'branchAndTag' => [
                'expected' => '{"path":"plugins-dist\/prefix","source":"https:\/\/github.com\/vendor\/prefix.git","branch":"1.0","tag":"v1.0.0"}',
                'branchTag' => [
                    'branch' => '1.0',
                    'tag' => 'v1.0.0',
                ],
            ],
        ];
    }

    #[DataProvider('dataJsonSerialization')]
    public function testJsonSerialization($expected, $branchTag)
    {
        // Given
        $specification = new Specification('prefix', \array_merge([
            'path' => SpipPaths::EXTENSIONS . '/prefix',
            'source' => 'https://github.com/vendor/prefix.git',
        ], $branchTag));
        $specification->setChanger(new RemoteUrlsStub(''));

        // When
        $actual = \json_encode($specification);

        // Then
        $this->assertSame($expected, $actual);
    }

    public function testCreateFromComposer()
    {
        // Given
        Platform::putEnv('COMPOSER', 'tests/Fixtures/create-from-composer/composer.json');
        $composer = ComposerFactory::create(new NullIO());

        // When
        $actual = Specification::createFromComposer($composer, 'test/prefix');

        // Then
        $this->assertSame('prefix', $actual->getPrefix());
        Platform::clearEnv('COMPOSER');
    }

    public function testFailCreateFromComposer()
    {
        // Given
        $this->expectException(InvalidSpecificationException::class);
        $this->expectExceptionMessage('Package "test/prefix" is not present.');

        // When
        Specification::createFromComposer($this->composer, 'test/prefix');

        // Then
        // An exception is thrown
    }
}
