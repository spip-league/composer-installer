<?php

namespace SpipLeague\Test\Composer\Fixtures;

use SpipLeague\Composer\Extensions\CollectionInterface;
use SpipLeague\Composer\Extensions\CollectionTrait;

class CollectionDummy implements CollectionInterface
{
    use CollectionTrait;

    public function getFile(): ?string
    {
        return \null;
    }
}
