<?php

namespace SpipLeague\Test\Composer\Fixtures;

use SpipLeague\Composer\Extensions\SpecificationInterface;

class SpecificationStub implements SpecificationInterface
{
    private string $prefix;

    private string $vendorName;

    private string $constraint;

    private string $path;

    public function __construct(
        string $prefix = '',
        string $vendorName = '',
        string $constraint = '',
        string $path = 'path',
    ) {
        $this->prefix = $prefix;
        $this->vendorName = $vendorName;
        $this->constraint = $constraint;
        $this->path = $path;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function computeVendorName(): string
    {
        return $this->vendorName;
    }

    public function computeConstraint(): string
    {
        return $this->constraint;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'path' => $this->path,
            'source' => 'source',
        ];
    }
}
