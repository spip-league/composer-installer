<?php

namespace SpipLeague\Test\Composer\Fixtures;

use SpipLeague\Composer\Extensions\CollectionInterface;
use Composer\Composer;
use SpipLeague\Composer\Switch\Operation\OperationInterface;

class OperationMock implements OperationInterface
{
    private string $message;

    private string $type;

    private bool $marked;

    private string $done;

    public function __construct(string $message, string $type, bool $marked, string $done)
    {
        $this->message = $message;
        $this->type = $type;
        $this->marked = $marked;
        $this->done = $done;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function mark(CollectionInterface $distribution, Composer $composer): ?OperationInterface
    {
        return $this->marked ? $this : \null;
    }

    public function do(CollectionInterface $distribution, Composer $composer): string
    {
        return $this->done;
    }
}
