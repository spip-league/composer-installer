<?php

namespace SpipLeague\Test\Composer\Fixtures;

use SpipLeague\Composer\Extensions\SpecificationInterface;

class SpecificationMock implements SpecificationInterface
{
    protected const URL = 'https://git-server/{vendorName}.git';

    private string $prefix;

    private string $vendorName;

    private string $constraint;

    private string $path;

    private string $source;

    public function __construct(
        string $prefix = 'prefix',
        string $vendorName = 'vendor/prefix',
        string $constraint = '1.0',
        string $path = 'path',
    ) {
        $this->prefix = $prefix;
        $this->vendorName = $vendorName;
        $this->constraint = $constraint;
        $this->path = $path;
        $this->source = \str_replace('{vendorName}', $this->vendorName, self::URL);
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function computeVendorName(): string
    {
        return $this->vendorName;
    }

    public function computeConstraint(): string
    {
        return '^' . $this->constraint;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'path' => $this->path,
            'source' => $this->source,
            'branch' => $this->constraint,
            'tag' => 'v' . $this->constraint . '.0',
        ];
    }
}
