<?php

namespace SpipLeague\Test\Composer\Fixtures;

use Composer\IO\NullIO;
use Composer\Util\ProcessExecutor;
use SpipLeague\Composer\Git\RemoteUrlsInterface;

class RemoteUrlsStub implements RemoteUrlsInterface
{
    /**
     * @var string[]
     */
    private array $ssh = [];

    /**
     * @var string[]
     */
    private array $https = [];

    public function __construct(string ...$urls)
    {
        while (!empty($urls)) {
            $this->ssh[] = \array_shift($urls);
            $this->https[] = !empty($urls) ? \array_shift($urls) : '';
        }
    }

    public function toSsh(string $url): string
    {
        return !empty($this->ssh) ? \array_shift($this->ssh) : '';
    }

    public function toHttps(string $url): string
    {
        return !empty($this->https) ? \array_shift($this->https) : '';
    }

    public function getProcessor(): ProcessExecutor
    {
        return new ProcessExecutor(new NullIO());
    }

    public function getRemote(string $path): string
    {
        return '';
    }

    public function setRemote(string $path, string $newUrl): string
    {
        return '';
    }
}
